FROM python:3.8
WORKDIR /app
ENV MODULE_NAME=app
ADD requirements.txt .
RUN python -m pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt && python3 -m spacy download fr_core_news_md
COPY . .
EXPOSE 80
CMD ["uvicorn", "app:app", "--host", "0.0.0.0", "--port", "80"]