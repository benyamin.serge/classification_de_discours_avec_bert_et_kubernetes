## Created two classes implementing an interface (factory design pattern) from training and execution  at runtime.

##These are French NLP models (CamemBERT fine tuned or Embeddings from spacy fr) used to classify long French speeches

I labelled manually some of the French speeches , which makes the data not so reliable but was one of the few datasets I could encounter easily on the net.
The data is on data folder on master branch.

## Then with Fastapi on docker and then on Kubernetes I deployed the Embedding Service with Google Cloud Platform.

Here first step , running in local mode the uvicorn app , seems to work :

![Uvicorn local](/images/Capture_d_%C3%A9cran_de_2021-11-13_09-36-04.png)

Then running it in docker in local ,with different text, seems to work:

![Docker local](/images/Capture_d_%C3%A9cran_de_2021-11-13_13-48-58.png)

Then I instanciate a kubernetes cluster , nodes appear to be ok :

![Kubectl get nodes](images/Capture_d_écran_de_2021-11-13_12-10-36.png)

Then we instanciate our deployment and service yamls , and we get the external IP adress :

![Kubectl get svc](images/external_IP_service_kubernetes.png)

Then we make a request to the provided IP adress:

![Request on kubernetes cluster](images/kubernetes_app_response.png)

It works ! :) We can visualize our app too running the IP in our browser:

![App visualization](images/app_kubernetes.png)

