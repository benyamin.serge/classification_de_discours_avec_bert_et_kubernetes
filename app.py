from fastapi import FastAPI
from models.classification import TextClassi
import pickle
import itertools
import spacy
import string
from spacy_lefff import LefffLemmatizer
from spacy.language import Language
import models.store.classifier as clf
from keras.models import model_from_json
import numpy as np
import os


app = FastAPI(title="Classification de discours",version="1.0")

def my_padding(mylist,pad_length):
    zeros = list(np.zeros(pad_length))
    if len(mylist)<pad_length:
        zeros[:len(mylist)] = mylist
        return zeros
    if len(mylist)>pad_length:
        return mylist[:pad_length]
    else:
        return mylist


@Language.factory('french_lemma')
def create_french_lemmatizer(nlp, name):
    return LefffLemmatizer()

@app.on_event('startup')
def load_model():
    clf.nlp = spacy.load("fr_core_news_md")
    json_file=open('models/store/classification_words_embedd_vmd3.json')
    loaded_model_json= json_file.read()
    json_file.close()
    clf.loaded_model=model_from_json(loaded_model_json)
    clf.loaded_model.load_weights('models/store/classification_words_embedd_vmd3.h5')
    list_tok_mlb= pickle.load(open('models/store/mlb_and_tokenizer_vmd3.pkl','rb'))
    clf.token= list_tok_mlb[0]
    clf.mlb = list_tok_mlb[1]
    clf.nlp.add_pipe('french_lemma')
    clf.french_lemmatizer = LefffLemmatizer(after_melt=True)




@app.post('/predict',tags = ['predictions'])
async def get_prediction(text: TextClassi):
    print('text',text)
    print('dict text',dict(text))
    data = dict(text)['data'][0][0]
    table= str.maketrans(dict.fromkeys(string.punctuation))
    data = data.translate(table)
    sentence = clf.nlp(data)
    sentence_lemm = [d._.lefff_lemma for d in sentence if d._.lefff_lemma != None]
    tokenized_sentence = list(itertools.chain(*clf.token.texts_to_sequences(sentence_lemm)))
    padded_sentence = my_padding(tokenized_sentence , 200)
    probas = clf.loaded_model.predict(np.expand_dims(padded_sentence,axis=1).T)[0]
    idx = np.argmax(probas)
    theme = clf.mlb.classes_[idx]

    return theme
    
@app.get('/infos', tags=['infos'])
def infofunc():
   return f'This is an API which classifies French speaks into these classes : {clf.mlb.classes_}'