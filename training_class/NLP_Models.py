from abc import ABC,abstractmethod

class NLP_Models(ABC):

    @abstractmethod
    def prepare(self):
        pass

    @abstractmethod
    def fit(self):
        pass

    @abstractmethod
    def predict(self):
        pass