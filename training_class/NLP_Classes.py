import spacy
import numpy as np
from tensorflow.python.client import device_lib
import tensorflow as tf
from sklearn.preprocessing import MultiLabelBinarizer,OneHotEncoder
from sklearn.model_selection import train_test_split
from keras.models import Model,Sequential
from keras.layers import Input,Embedding,LSTM,Dense,Dropout,Flatten
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.initializers import Constant
import string
from spacy_lefff import LefffLemmatizer
from spacy.language import Language
import itertools
from NLP_Models import NLP_Models
from transformers import CamembertTokenizer,TFCamembertModel,CamembertModel

class Word_Embedding(NLP_Models):

    def __init__(self , df,target,data_col,MAX_LEN,dimension,epochs,batch_size):
        """

        :param df: the input dataframe (pd.DataFrame)
        :param target: the target vector (np.array)
        :param data_col: name of the column where the speech is (str)
        :param MAX_LEN: size of the total sentence  (including padding) (int
        :param dimension: max dimension of the vector space (int)
        :param epochs: number of epochs for training (int)
        :param batch_size: batch number for training (int)
        """
        self.df = df
        self.target = target
        self.data_col = data_col
        self.MAX_LEN= MAX_LEN
        self.dimension= dimension
        self.epochs = epochs
        self.batch_size = batch_size

    @staticmethod
    def keep_before_signature(s, pattern):
        """
        BAsically removes the signature at the end of the speech
        :param s: string
        :param pattern: pattern to remove
        :return: a string without signature
        """
        try:
            idx = s.index(pattern)
            return s[:idx]
        except:
            return s

    @Language.factory('french_lemma')
    def create_french_lemmatizer(nlp, name):
        return LefffLemmatizer()

    def create_nlp(self):
        """
        create spacy nlp and french lemmatizer
        :return: a french lemmatizer
        """
        self.nlp = spacy.load("fr_core_news_sm")
        self.nlp.add_pipe('french_lemma')
        self.french_lemmatizer = LefffLemmatizer(after_melt=True)


    def Clean_prepare_sentence(self, sentence):
        """
        remove question answer tags ,punctuation  , lemmatize
        :param sentence:
        :return: sentence without punctuation and lemmatized
        """
        table = str.maketrans(dict.fromkeys(string.punctuation))
        sentence = sentence.translate(table)
        sentence = sentence.replace('Q - ', '').replace('R - ', '').lower()
        sentence = self.nlp(sentence)
        sentence_lemm = [d._.lefff_lemma for d in sentence if d._.lefff_lemma != None]
        return sentence_lemm

    def transform_target(self):
        """
        create multi class labels
        :return: vector with multi class labels
        """
        self.mlb = MultiLabelBinarizer()
        self.target_vector = self.mlb.fit_transform(self.df[self.target].map(lambda x: [x]))

    def split_n_tokenize(self):
        """
        split in train test ,tokenize & pad the sentence
        :return: a padded tokenized sequence
        """
        lemm_table = self.df[self.data_col].map(lambda x: self.Clean_prepare_sentence(self.nlp, x))
        self.X_train, self.X_test, self.Y_train, self.Y_test = train_test_split(lemm_table, self.target_vector)
        self.tok = Tokenizer()
        self.tok.fit_on_texts(self.X_train)
        tokenized = self.tok.texts_to_sequences(self.X_train)
        self.len_vocab = len(self.tok.index_word.keys())
        self.padded = pad_sequences(tokenized, maxlen=self.MAX_LEN, truncating='post', padding='post')

    @staticmethod
    def get_available_gpus():
        local_device_protos = device_lib.list_local_devices()
        return [x.name for x in local_device_protos if x.device_type == 'GPU']

    def create_embedding_matrix(self):
        """
        create embedding matrix with spacy for ou vocabulary
        :return: an embedding matrix for the current vocabulary
        """
        embed = np.zeros((self.len_vocab, self.dimension))
        for i in range(len(self.tokenizer.index_word.keys())):
            embed[i, :] = self.nlp(self.tok.index_word[i + 1]).vector[:self.dimension]
        return embed

    def prepare(self):
        """
        chain all the above method to get a lemmnatized , without punctuation , tokenized and padded sequence
        Then create an embedding matrix based on the tokens
        :return:
        """
        self.df[self.data] = self.df[self.data].map(lambda x: Word_Embedding.keep_before_signature(x, 'Source')).map(
            lambda x: Word_Embedding.keep_before_signature(x, 'source'))
        self.create_nlp()
        self.transform_target()
        self.split_n_tokenize()
        self.embed = self.create_embedding_matrix(self.len_vocab, self.tok, self.dimension)

    def create_model(self):
        """
        create a model with the emeddings
        :return: a model with dimension the number of classes
        """
        mymodel = Sequential()
        embedding = Embedding(self.len_vocab, self.dimension, embeddings_initializer=Constant(self.embed),
                              trainable=False)
        mymodel.add(embedding)
        mymodel.add(LSTM(128, activation='tanh', name='lstm_layer'))
        mymodel.add(Dropout(0.3))
        mymodel.add(Dense(64, activation='relu', name='dense_layer3'))
        mymodel.add(Dropout(0.3))
        mymodel.add(Dense(32, activation='relu', name='dense_layer4'))
        mymodel.add(Dropout(0.3))
        mymodel.add(Dense(len(self.mlb.classes_), activation='softmax', name='dense_layer5'))
        return mymodel

    def Train(self,X_train, Y_train,  epochs=10, batch_size=20):
        """
        train the model
        :param X_train: padded and tokenized sequence
        :param Y_train: multilabel target
        :param epochs: number of epochs
        :param batch_size: batch size number
        :return: a trained model
        """
        model = self.create_model(self.len_vocab, self.embed)
        model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
        model.summary()
        model.fit(X_train, Y_train, epochs=epochs, batch_size=batch_size)
        return model

    def fit(self):
        """
        call training of the model
        :return: a fitted model
        """
        self.model = self.Train(self.padded , self.Y_train,self.embed,epochs = self.epochs, batch_size = self.batch_size)

    @staticmethod
    def mypadding(mylist, lent):
        zeros = list(np.zeros(lent))
        if len(mylist) > lent:
            return mylist[:lent]
        if len(mylist) < lent:
            zeros[:len(mylist)] = mylist
            return zeros
        else:
            return mylist

    def predict(self ,sentence):
        """
        predicts the theme of the sentence
        sentence : an input sentence (string)
        :return: a tokenized and padded sentence , up to max len
        """
        tok_sentence= list(itertools.chain(*self.tok.texts_to_sequences(Word_Embedding.Clean_prepare_sentence(self.nlp, sentence))))
        padded_test = Word_Embedding.mypadding(tok_sentence,self.MAX_LEN)
        index = np.argmax(self.model.predict(np.expand_dims(padded_test, axis=1).T)[0])
        return index


class Bert_Tuning(NLP_Models):
    """
    Class using Bert model
    """
    def __init__(self , df,target,data_col,MAX_LEN,dimension,epochs,batch_size):
        self.df = df
        self.target = target
        self.data_col = data_col
        self.MAX_LEN= MAX_LEN
        self.dimension= dimension
        self.epochs = epochs
        self.batch_size = batch_size

    def encode_sentence(self, sentence):
        """

        :param sentence: a sentence to encode
        :return:
        """
        token_ids = np.zeros(shape=(len(sentence), self.MAX_LEN),
                             dtype=np.int32)
        for i, s in enumerate(sentence):
            encoded = self.tokenizer.encode(s, max_length=self.MAX_LEN)
            token_ids[i, 0:len(encoded)] = encoded
        attention_mask = (token_ids != 0).astype(np.int32)
        return {"input_ids": token_ids, "attention_mask": attention_mask}

    def prepare(self):
        """
        prepare sentence using the camembert tokenizer
        :return: a dictionnary of values for train ,test ,eval for camembert
        """
        self.df[self.data] = self.df[self.data].map(lambda x: Word_Embedding.keep_before_signature(x, 'Source')).map(
            lambda x: Word_Embedding.keep_before_signature(x, 'source'))
        Wembedding = Word_Embedding(self.df,self.target,self.data_col,self.MAX_LEN,self.dimension,
                                    self.epochs,self.batch_size)
        Wembedding.create_nlp()
        Wembedding.transform_target()
        lemm_table = self.df[self.data_col].map(lambda x: Wembedding.Clean_prepare_sentence(self.nlp, x))
        self.X_train, self.X_test, self.Y_train, self.Y_test = train_test_split(lemm_table, self.target_vector)
        model_name = "camembert-base"
        self.tokenizer = CamembertTokenizer.from_pretrained(model_name)
        self.dic_train = self.encode_sentence(self.X_train)
        self.dic_test = self.encode_sentence(self.X_test)
        self.dic_val = self.encode_sentence( self.X_val )

    def create_model(self,bert_model):
        """
        We freeze all the layers and add a dense to fine tune Bert
        :param bert_model:
        :return: a model
        """
        input_layer = Input(shape=(self.MAX_LEN,), dtype=tf.int32)
        bert_model.layers[0].trainable = False
        bert = bert_model.roberta(input_layer)
        bert = bert[0]
        flat = Flatten()(bert)
        sec_layer = Dense(units=64, activation='relu')(flat)
        dropout_2 = Dropout(0.3)(sec_layer)
        third_layer = Dense(units=64, activation='relu')(dropout_2)
        dropout_3 = Dropout(0.3)(third_layer)
        fourth_layer = Dense(units=32, activation='relu')(dropout_3)
        dropout_4 = Dropout(0.3)(fourth_layer)
        classifier = Dense(len(self.mlb.classes_), activation='softmax')(dropout_4)
        model = Model(inputs=input_layer, outputs=classifier)

        return model

    def fit(self):
        """
        fits the above mentionned model
        :return: a fitted model
        """
        bert_model = TFCamembertModel.from_pretrained("camembert-base")
        opt = tf.keras.optimizers.Adam(learning_rate=5e-6, epsilon=1e-08)
        loss_fn = tf.keras.losses.CategoricalCrossentropy()
        self.model = self.create_model(bert_model, self.MAX_LEN)
        self.model.compile(optimizer=opt, loss=loss_fn, metrics=['accuracy'])
        self.model.summary()
        self.model.fit(
            self.dic_train['input_ids'], self.target_vector, epochs=self.epochs, batch_size=self.batch_size,
            verbose=1
        )

    def predict(self,sentence):

        test_point = self.encode_sentence(sentence)
        return self.mlb.classes_[np.argmax(self.model.predict(test_point['input_ids'])[0])]


