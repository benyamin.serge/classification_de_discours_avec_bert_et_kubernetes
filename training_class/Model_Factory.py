from NLP_Classes import Word_Embedding,Bert_Tuning

class Model_Factory():
    """
    THe idea here is to fit and predict with whatever type of model the user wants
    At run time, the user will call:
    md_factory = Model_Factory()
    model = md_factory.create_nlp_model('desired model',params for constructor)
    model.prepare()
    model.fit()
    model.predict()
    """
    def create_nlp_model(self,name,params):
        if name=='Embeddings':
            return Word_Embedding(df = params['df'],target = params['target'],
                           data_col = params['data_col'],MAX_LEN = params['MAX_LEN'],
                           dimension = params['dimension'],epochs=params['epochs'],
                           batch_size = params['batch_size'])
        if name=='Bert':
            return Bert_Tuning(df = params['df'],target = params['target'],
                           data_col = params['data_col'],MAX_LEN = params['MAX_LEN'],
                           dimension = params['dimension'],epochs=params['epochs'],
                           batch_size = params['batch_size'])