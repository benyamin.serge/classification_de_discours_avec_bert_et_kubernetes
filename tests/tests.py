import pandas as pd
from keras.models import model_from_json
import subprocess

class Test:
    def __init__(self):
        self.json_adress = 'models/store/classification_words_embedd_v3.json'
        self.weight_adress = 'models/store/classification_words_embedd_v3.h5'
        self.docker_image_name = 'myfastapp'

    def test_json_presence(self):
        json_file = open(self.json_adress)
        assert(json_file != None)
        return json_file

    def test_weight_presence(self ):
        json_file = self.test_json_presence()
        loaded_model_json = json_file.read()
        json_file.close()
        loaded_model = model_from_json(loaded_model_json)
        loaded_model.load_weights(self.weight_adress)
        assert(loaded_model != None)

    def assert_image_presence(self):
        subprocess.call('sudo docker images myfastapp > mydocker_list.csv',shell=True)
        df = pd.read_csv('mydocker_list.csv')
        assert(self.docker_image_name in df.iloc[0].values[0])
