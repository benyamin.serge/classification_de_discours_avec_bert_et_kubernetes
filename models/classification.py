from pydantic import BaseModel,conlist
from typing import List

class TextClassi(BaseModel):
    data: List[conlist(str, min_items =1,max_items=1)]